<?php

use Illuminate\Database\Seeder;
use App\Dealers;

class DealersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Dealers::create(['names' => 'Super Autos', 'cities_id' => 1]);
        Dealers::create(['names' => 'Avila', 'cities_id' => 2]);
        Dealers::create(['names' => 'El Tocuyo', 'cities_id' => 3]);
        Dealers::create(['names' => 'San martin', 'cities_id' => 3]);
        Dealers::create(['names' => 'Tu carro', 'cities_id' => 3]);
        Dealers::create(['names' => 'Bello monte', 'cities_id' => 3]);
        Dealers::create(['names' => 'Cars', 'cities_id' => 3]);
        Dealers::create(['names' => 'Ruficar', 'cities_id' => 3]);
        Dealers::create(['names' => 'Erios Cars', 'cities_id' => 3]);
        Dealers::create(['names' => 'Obelisco Cars', 'cities_id' => 3]);
        Dealers::create(['names' => 'Manaos Cars', 'cities_id' => 3]);
        Dealers::create(['names' => 'Bocono cars', 'cities_id' => 3]);
        Dealers::create(['names' => 'Margarita cars', 'cities_id' => 3]);
        Dealers::create(['names' => 'Puerto cabello Cars', 'cities_id' => 3]);
        Dealers::create(['names' => 'Aragua Cars', 'cities_id' => 3]);
    }
}
