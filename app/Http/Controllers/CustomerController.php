<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Customer;

class CustomerController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Retorna la lista de los clientes asociados a un concesionario
        $customer = Customer::where('user_id', auth()->id())->get();
        $result = array();
        foreach ($customer as $value) {
            # code...
            $value->dealer = $value->dealer->names;            
            $result[] = $value;
        }
        return $result;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $customer = new Customer;
        $customer->names = $request->nombre;
        $customer->surname = $request->apellido;
        $customer->cedula = $request->cedula;
        $customer->telefono = $request->telefono;
        $customer->email = $request->email;
        $customer->active = true;
        $customer->user_id = auth()->id();
        $customer->dealer_id = $customer->user_id;  //El cliente pertenece al concesionario de la persona autenticada
        $customer->save();

        return $customer;
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $customer = Customer::findOrFail($id);
        $customer->fill($request->all());
        $customer->update();

        return $customer;
    }


        /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $customer = Customer::findOrFail($id);

        return $customer;
    }

    /**
     * Inactive the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function inactivar($id)
    {
        $customer = Customer::findOrFail($id);
        $customer->active = false;
        $customer->save();
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $customer = Customer::find($id);
        $customer->active = false;
        $customer->save();        
        $customer->delete();
    }
}
