@extends('layouts.app')

@section('content')
<div id="crud" class="row">

    <div class="col-xs-12">
        <h1 class="page-header"> MIS CLIENTES </h1>
    </div>

    <div class="col-sm-7">
        <a href="#" class="btn btn-primary pull-right"> Nuevo cliente</a>
        <table class="table table-hover table-striped">
            <thead>
                <tr>
                    <th>ID</th>
                    <th>Cliente</th>
                    <th colspan="2">
                        &nbsp;
                    </th>

                </tr>
            </thead>
            <tbody>
                <tr v-for="cliente in clientes">
                    <td widht="10px"> @{{ cliente.id }}</td>
                    <td> @{{ cliente.names }} </td>
                    <td widht="10px">
                       <a href="#" class="btn btn-warning btn-sm">Editar</a>
                    </td>
                    <td widht="10px">
                        <a href="#" class="btn btn-danger btn-sm" v-on:click.prevent="deleteCliente(cliente)">Eliminar</a>
                     </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div class="col-sm-5">
        <pre>
            @{{ $data }}
        </pre>
    </div>

</div>
@endsection
