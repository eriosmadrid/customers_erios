<?php

namespace App;

use app\Cities;
use Illuminate\Database\Eloquent\Model;

class Dealers extends Model
{
    public function citie() {
        return $this->belongsTo(Cities::class, 'cities_id');
    }

}
