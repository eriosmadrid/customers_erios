<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Customer;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); 

*/

// --------  Rutas api para los clientes -------------
//Listar los clientes
Route::get('customers', function () {
    $customer = Customer::get();
    return $customer;
});

//Obtener un cliente

Route::get('customers/{id}', function($id) {
    $customer = Customer::findOrFail($id);
    return $customer;
});


//Registrar cliente
Route::post('customers', function (Request $request) {

    $request->validate([
        'nombres' => 'required|max:50',
        'apellidos' => 'required|max:50',
        'cedula' => 'required|max:20',
        'email' => 'required|max:100|email|unique:customers',
        'sexo' => 'required|max:40',
        'estado_civil' => 'required|max:50',
        'telefono' => 'required|numeric',
    ]);
    
    $customer = new Customer;
    $customer->names = $request->input('nombres');
    $customer->surname = $request->input('apellidos');
    $customer->cedula = $request->input('cedula');
    $customer->email = $request->input('email');
    $customer->sexo = $request->input('sexo');
    $customer->estado_civil = $request->input('estado_civil');
    $customer->telefono = $request->input('telefono');
    $customer->dealer_id = $request->input('dealer_id');

    $customer->save();
    return "Cliente creado exitosamente";
});

//Actualizar cliente
Route::put('customer/{id}', function (Request $request, $id) {
    $request->validate([
        'nombres' => 'required|max:50',
        'apellidos' => 'required|max:50',
        'cedula' => 'required|max:20',
        'email' => 'required|max:100|email|unique:customers,email' .$id,
        'sexo' => 'required|max:40',
        'estado_civil' => 'required|max:50',
        'telefono' => 'required|numeric',
    ]);

    $customer = Customer::findOrFail($id);
    $customer->names = $request->input('nombres');
    $customer->surname = $request->input('apellidos');
    $customer->cedula = $request->input('cedula');
    $customer->email = $request->input('email');
    $customer->sexo = $request->input('sexo');
    $customer->estado_civil = $request->input('estado_civil');
    $customer->telefono = $request->input('telefono');
    $customer->dealer_id = $request->input('dealer_id');    
    $customer->save();
    return "Cliente actualizado exitosamente";
});

//Eliminar cliente
Route::patch('customer/{id}', function($id) {
    $customer = Customer::findOrFail($id);
    $customer->active = false;
    $customer->save();
    return "Cliente Eliminado exitosamente";
});

//----------------------------------------------------