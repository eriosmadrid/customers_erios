<?php

use Illuminate\Database\Seeder;
use App\Cities;

class CitiesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Cities::create(['names' => 'Puerto Ordaz']);
        Cities::create(['names' => 'Caracas']);
        Cities::create(['names' => 'Barquisimeto']);
        Cities::create(['names' => 'Puerto la cruz']);
        Cities::create(['names' => 'Valencia']);
        Cities::create(['names' => 'Maracaibo']);
        Cities::create(['names' => 'Merida']);
        Cities::create(['names' => 'San cristobal']);
        Cities::create(['names' => 'Trujillo']);
        Cities::create(['names' => 'Barinas']);
        Cities::create(['names' => 'Maturin']);
        Cities::create(['names' => 'Bocono']);
        Cities::create(['names' => 'Margarita']);
        Cities::create(['names' => 'Puerto cabello']);
        Cities::create(['names' => 'Aragua']);
    }
}
