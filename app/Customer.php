<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Dealers;
class Customer extends Model
{
    use SoftDeletes;
    protected $table = 'customers';
    protected $fillable = [
        'names', 'surname', 'cedula', 'telefono', 'email'
    ];

    public function dealer() {
        return $this->belongsTo(Dealers::class, 'dealer_id');
    }    

}
